# linux-configs

The group of files that represent my actual configuration.

Contains:

 - a script to setup KDE Plasma integration with i3-wm;
 - a script to auto download default application
 - bashrc path export
 - various configurations
 
## Credits:
 
 - [Plasma + i3:]
    + [https://github.com/avivace/dotfiles]
    + [https://github.com/nightsh/i3-plasma]