#!/bin/bash

# made the wifi work
rmmod ideapad_laptop

# upgrade the system
apt-get update
apt-get upgrade
apt-get autoremove

# add the wifi bad module to blacklist
cp /etc/modprobe.d/blacklist.conf /etc/modprobe.d/blacklist.conf.old
printf "# block wifi bad module\nblacklist ideapad_laptop\n" >> /etc/modprobe.d/blacklist.conf

# install and check nvidia driver
apt install nvidia-driver-390 nvidia-prime
# prime-select query (should return nvidia)
# sudo prime-select intel (should say “selected intel profile” or similar)
# prime-select query (should return intel)

# download dot files
# TODO

# i3-KDE integration
	# install i3-wm
	apt-get install i3-wm
	apt-get install i3

	# add i3-gaps
	cd /usr/local/bin/
		# clone the repository
		git clone https://www.github.com/Airblader/i3 i3-gaps
		cd i3-gaps

		# compile & install
		apt-get install autoconf
		autoreconf --force --install
		rm -rf build/
		mkdir -p build && cd build/

		# Disabling sanitizers is important for release versions!
		# The prefix and sysconfdir are, obviously, dependent on the distribution.
		sudo apt-get install libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev \
		libxcb-util0-dev libxcb-icccm4-dev libyajl-dev \
		libstartup-notification0-dev libxcb-randr0-dev \
		libev-dev libxcb-cursor-dev libxcb-xinerama0-dev \
		libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev \
		autoconf libxcb-xrm0 libxcb-xrm-dev automake libxcb-shape0-dev
		../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers
		make
		make install
	# clone .bashrc for path addition

	# disable the ksplashqml autostart
	sudo mv /usr/bin/ksplashqml /usr/bin/ksplashqml.old

	# create i3 launcher
	mkdir .config/plasma-workspace/
	mkdir .config/plasma-workspace/env/
	echo "export KDEWM=/usr/bin/i3" > ~/.config/plasma-workspace/env/set_window_manager.sh

	# clone .config/i3/config for bar hiding
