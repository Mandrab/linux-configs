#!/bin/bash

# update all
apt-get update
apt-get upgrade
apt-get autoremove

# install background manager
apt-get install feh

# python 2
apt-get install python

# latex compiler
apt-get install latexmk
# latex package
apt-get install texlive-base 		\
	texlive-bibtex-extra		\
	texlive-extra-utils 		\
	texlive-lang-european 		\
	texlive-latex-extra 		\
	texlive-latex-recommended	\
	texlive-science

snap install --classic vscode
	# platformio
	# latex workshop
snap install --classic eclipse
snap install --classic android-studio

# staruml

# wine
apt-get install wine
rm -rf ~/.wine
winecfg

# .net
	# Register Microsoft key and feed
	wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
	dpkg -i packages-microsoft-prod.deb

	# Install the .NET SDK
	sudo add-apt-repository universe
	sudo apt-get install apt-transport-https
	sudo apt-get update
	sudo apt-get install dotnet-sdk-2.2
